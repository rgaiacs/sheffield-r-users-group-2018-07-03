#include <stdlib.h>
#include <stdio.h>

// Function declaration.
// GFortran adds _ at the end of the name of the subroutine.
void sum_(float *x, float *y, float *total);

int main()
{
	float x;
	float y;
	float total;

	x = 1.0;
	y = 1.0;
	sum_(&x, &y, &total);  // Call to Fortran subroutine.

	printf("x     = %f\n", x);
	printf("y     = %f\n", y);
	printf("total = %f\n", total);

	return 0;
}
